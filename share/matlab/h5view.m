T   = h5read('seq.h5','/seqdiag/T');           % temporal sampling points
RXP = h5read('seq.h5','/seqdiag/RXP');         % RF Receiver phase; unit: radiants; if negative, the TPOI was not an ADC
TXM = h5read('seq.h5','/seqdiag/TXM');         % RF Transmitter magnitude
TXP = h5read('seq.h5','/seqdiag/TXP');         % RF Transmitter phase; unit: radiants
GX  = h5read('seq.h5','/seqdiag/GX');          % physical X-Gradient
GY  = h5read('seq.h5','/seqdiag/GY');          % physical Y-Gradient
GZ  = h5read('seq.h5','/seqdiag/GZ');          % physical Z-Gradient

tmax=2000;
figure(1)
subplot(3,2,1)
plot(T(1:tmax), RXP(1:tmax))
subplot(3,2,2)
plot(T(1:tmax), TXM(1:tmax))
subplot(3,2,3)
plot(T(1:tmax), TXP(1:tmax))
subplot(3,2,4)
plot(T(1:tmax), GX(1:tmax))
subplot(3,2,5)
plot(T(1:tmax), GY(1:tmax))
subplot(3,2,6)
plot(T(1:tmax), GZ(1:tmax))
