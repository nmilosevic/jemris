function niftiToJemrisMPM()
% translate nifti image to jemris readable format and save as sample.h5
r1_file = '/data/pt_np-kpodranski/projects/reco_test_nliv/kerrin_/SENSE/Results/2019-08-23-105428_v6Recon_10pcMask_8kBeta_Echo_01_Magnitude_R1.nii';
one_slice = true;
BrainSample.T1 = double(permute(niftiread(r1_file),[1 3 2]));
BrainSample.T1 = BrainSample.T1(200,:,:);
BrainSample.T1(BrainSample.T1~=0)=1/BrainSample.T1(BrainSample.T1~=0);
dims = size(BrainSample.T1);

%% plot slice or visualize volume
if one_slice
    imshow(reshape(BrainSample.T1, [dims(2), dims(3)]),[]);
    %imshow(reshape(BrainSample.T2S, [dims(1), dims(2)]), []);
else
    sizeO = size(BrainSample.T1);
    figure;
    slice(double(BrainSample.T1),sizeO(2)/2,sizeO(1)/2,sizeO(3)/2);
    colormap gray;
    title('Original');
end
