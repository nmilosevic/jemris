function basicRecon(file, dims, echos, ifft)
%% unpack hdf5 signal file into M with size [n_timepoints, 3]
info = h5info(file);
channels = numel(info.Groups(1).Groups(1).Datasets);
t = h5read (file, sprintf('/signal/times'));
[t,I]=sort(t);
for i=1:channels
    A = ( h5read (file, sprintf('/signal/channels/%02i',i-1)) )';
    M(:,:,i) = A(I,:);
end

%% reconstruct
x = dims(1);
y = dims(2);
z = dims(3);

for j=1:channels
    for i=1:echos
        X = reshape(M,[z,echos,y,x,3,channels]);
        X = reshape(X(:,i,:,:,:,j), [z,y,x,3]);  % only i-th echo
        X = complex(reshape(X(:,:,:,1), [z,y,x]), reshape(X(:,:,:,2), [z,y,x]));  % Mx+iMy
        if mod(i,2)==1
            X = flip(X, 1);
        end
        if ifft
            X = ifftn(X);
            X = ifftshift(X);
        end
        subplot(echos,channels,i*j);
        imshow(abs(reshape(X(:,:,ceil(x/2)), [z,y])), []);
    end
end
