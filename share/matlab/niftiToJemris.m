function niftiToJemris(niftifile, interp_factor)
% translate nifti image to jemris readable format and save as sample.h5
%% config
add_susceptiblity = false;
one_slice = true;
M0 = 1;
T1 = 1;
T2 = 1;
T2S = 1;
CS = 1;
info = niftiinfo(niftifile);
RES = info.PixelDimensions/(2^interp_factor)
OFFSET = [0, 0, 0];
% info = niftiinfo(niftifile);

%% define tissuse parameters
%        T1  T2 T2*[ms]  M0 CS[rad/sec]      Label
tissue=[2569 329  158   1.00   0         ;  % 1 = CSF
         833  83   69   0.86   0         ;  % 2 = GM
         500  70   61   0.77   0         ;  % 3 = WM
         350  70   58   1.00 220*2*pi    ;  % 4 = Fat (CS @ 1.5 Tesla)
         900  47   30   1.00   0         ;  % 5 = Muscle / Skin
        2569 329   58   1.00   0         ;  % 6 = Skin
           0   0    0   0.00   0         ;  % 7 = Skull
         833  83   69   0.86   0         ;  % 8 = Glial Matter
         500  70   61   0.77   0         ;];% 9 = Meat

%% create parameter maps
% preprocessing of brain segmentation
BRAIN = double(round(permute(niftiread(niftifile),[1 3 2])*0.0196080));
BRAIN = interp3(BRAIN, interp_factor, 'nearest');
if interp_factor
    BRAIN = padarray(BRAIN, [1,1,1], 'pre');
end
whos BRAIN
dims = size(BRAIN);

if one_slice
    %BRAIN = BRAIN(200*2^interp_factor,:,:);
    BRAIN = BRAIN(:,:,350*2^interp_factor);
end


PARAMS={'M0','T1','T2','T2S','DB'};
fact=[M0 T1 T2 T2S CS];
INDEX =[4 1 2 3 5];
for i=1:9
 for j=1:5
  if i==1,eval(['BrainSample.',PARAMS{j},'=zeros(size(BRAIN));']);end
  I   = find(BRAIN==i);
  ind = INDEX(j);
  eval(['BrainSample.',PARAMS{j},'(I)=fact(j)*tissue(i,ind);']);
 end
end

%add suceptibility (gamBo must be defined above)
if add_susceptiblity
  BrainSample.DB = BrainSample.DB + 2*pi*1e6*DB*gamBo;
end

BrainSample.RES = RES;
BrainSample.OFFSET = OFFSET;

% Write sample to hdf5 format. This function is provided by jemris.
writeSample(BrainSample);

%% plot slice or visualize volume
if one_slice
    %imshow(reshape(BrainSample.M0, [dims(2), dims(3)]), []);
    imshow(reshape(BrainSample.T2S, [dims(1), dims(2)]), []);
else
    sizeO = size(BrainSample.T1);
    figure;
    slice(double(BrainSample.T1),sizeO(2)/2,sizeO(1)/2,sizeO(3)/2);
    colormap gray;
    title('Original');
end
